import 'package:flutter/material.dart';

import 'package:whereismycheese/screens/home/map.dart';
import 'package:whereismycheese/screens/home/cheesy_note.dart';
import 'package:whereismycheese/screens/home/cheesy_note_dialog.dart';
import 'package:whereismycheese/services/auth.dart';

import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter_background_geolocation/flutter_background_geolocation.dart'
    as bg;

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with WidgetsBindingObserver {
  final AuthService _auth = AuthService();
  bool _showCheesyNoteDialog = false;
  bool showNote = false;
  String noteId;

  Marker latestMarker;
  bg.GeofenceEvent geofenceEvent;


  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    _onGeofence();
  }

  void _onGeofence() async {
    bg.BackgroundGeolocation.onGeofence((bg.GeofenceEvent event) {
      print("GEOFENCE EVENT !!!");
      if (event.action == "ENTER") {
        setState(() {
          showNote = true;
          noteId = event.identifier.toString();
        });
      }
    });

    bg.State state = await bg.BackgroundGeolocation.state;
    print(state);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "WhereIsMyCheese",
          style: TextStyle(color: Colors.white),
        ),
        elevation: 0.0,
        actions: <Widget>[
          FlatButton(
            child: Text(
              "sign out",
              style: TextStyle(color: Colors.white),
            ),
            onPressed: () async {
              _auth.signOut();
            },
          )
        ],
      ),
      body: Stack(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            child: Map(
              onCheesyDialogClose: () {
                setState(
                  () => _showCheesyNoteDialog = !_showCheesyNoteDialog,
                );
              },
              setLatestMarker: (Marker marker) {
                setState(() => latestMarker = marker);
              },
            ),
          ),
          showNote
              ? CheesyNote(
                  markerId: noteId,
                  onCheesyNoteClose: () {
                    setState(() {
                      showNote = false;
                    });
                  },
                )
              : SizedBox(
                  height: 0.0,
                ),
          _showCheesyNoteDialog
              ? CheesyNoteDialog(
                  onCheesyDialogClose: () {
                    setState(() {
                      _showCheesyNoteDialog = false;
                    });
                  },
                  point: latestMarker.position,
                  markerId: latestMarker.markerId,
                )
              : SizedBox(),
          // _geoFenceEnteredAlert(),
        ],
      ),
    );
  }
}
