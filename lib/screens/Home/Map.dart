import 'package:flutter/material.dart';

import 'package:whereismycheese/shared/loading.dart';
import 'package:whereismycheese/services/database.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'dart:math';
import 'package:geolocator/geolocator.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_background_geolocation/flutter_background_geolocation.dart'
    as bg;

class Map extends StatefulWidget {
  final VoidCallback onCheesyDialogClose;
  final Function(Marker) setLatestMarker;

  Map({
    @required this.onCheesyDialogClose,
    @required this.setLatestMarker,
  });

  @override
  _MapState createState() => _MapState();
}

class _MapState extends State<Map> {
  DatabaseService _db = DatabaseService();
  BitmapDescriptor pinLocationIcon;
  Position position;
  bool positionLoaded = false;

  List<Marker> _allMarkers = [];
  Marker latestMarker;

  void initState() {
    super.initState();

    Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
        .then((Position myPos) {
      setState(() {
        position = myPos;
        positionLoaded = true;
      });
    });

    bg.BackgroundGeolocation.ready(bg.Config(
      desiredAccuracy: bg.Config.DESIRED_ACCURACY_HIGH,
      distanceFilter: 1.0,
      stopOnTerminate: false,
      startOnBoot: true,
      debug: true,
      logLevel: bg.Config.LOG_LEVEL_VERBOSE,
    )).then((bg.State state) {
      if (!state.enabled) {
        bg.BackgroundGeolocation.start();
      }
    });

    _onGeofenceEntered();

    BitmapDescriptor.fromAssetImage(
            ImageConfiguration(), 'assets/images/cheese32.png')
        .then((onValue) {
      pinLocationIcon = onValue;
    });
  }

  Marker _generateNewMarker(LatLng point) {
    String genId = Random().nextDouble().toString();
    MarkerId markerId = MarkerId(genId);
    return Marker(
      markerId: markerId,
      position: point,
      visible: true,
      icon: pinLocationIcon,
    );
  }

  Marker _dBCoordsToMarker(LatLng point, String markerId) {
    MarkerId newMarkerId = MarkerId(markerId);
    return Marker(
      markerId: newMarkerId,
      position: point,
      zIndex: 1.0,
      visible: true,
      icon: pinLocationIcon,
    );
  }

  void _handleLongPress(LatLng point) {
    Marker longPressPoint = _generateNewMarker(point);

    setState(() {
      _allMarkers.add(longPressPoint);
    });

    widget.setLatestMarker(longPressPoint);
    widget.onCheesyDialogClose();
  }

  Set<Marker> _getMarkers(AsyncSnapshot<QuerySnapshot> snapshot) {
    Marker currentMarker;
    List<Marker> markers = [];
    List<DocumentSnapshot> items = snapshot.data.documents;

    if (snapshot.hasData) {
      for (DocumentSnapshot item in items) {
        print("ITEM IS ${item.data}");
        if (item.data["latitude"] != null) {
          LatLng locationAsLatLng =
              LatLng(item.data["latitude"], item.data["longitude"]);
          currentMarker = _dBCoordsToMarker(locationAsLatLng, item.documentID);
          markers.add(currentMarker);
          _generateGeofence(currentMarker.position, currentMarker.markerId);
        }
      }

      return Set.from(markers);
    }

    return null;
  }

  void _generateGeofence(LatLng point, MarkerId id) {
    bg.BackgroundGeolocation.addGeofence(bg.Geofence(
      identifier: id.value,
      radius: 15.0,
      latitude: point.latitude,
      longitude: point.longitude,
      notifyOnEntry: true,
    )).then((bool success) {
      print("successfully added geofence!");
    }).catchError((error) {
      print("failed adding geofence");
    });
  }

  void _onGeofenceEntered() {
    bg.BackgroundGeolocation.onGeofence((e) => print('entered a geofence $e'));
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: _db.getMarkers(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasData) {
          return positionLoaded ? GoogleMap(
            buildingsEnabled: true,
            initialCameraPosition: CameraPosition(
              target: LatLng(
                position.latitude,
                position.longitude,
              ),
              zoom: 18.0,
            ),
            myLocationButtonEnabled: true,
            myLocationEnabled: true,
            onLongPress: _handleLongPress,
            markers: _getMarkers(snapshot),
          ) : Center(
            child: CircularProgressIndicator(),
          );
        } else {
          return Loading();
        }
      },
    );
  }
}
