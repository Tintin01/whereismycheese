import 'package:flutter/material.dart';
import 'package:whereismycheese/services/database.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class CheesyNoteDialog extends StatefulWidget {
  final VoidCallback onCheesyDialogClose;
  final LatLng point;
  final MarkerId markerId;

  CheesyNoteDialog({
    @required this.onCheesyDialogClose,
    @required this.point,
    @required this.markerId,
  });

  @override
  _CheesyNoteDialogState createState() => _CheesyNoteDialogState();
}

class _CheesyNoteDialogState extends State<CheesyNoteDialog> {
  final DatabaseService _db = DatabaseService();
  String _note = "";
  bool isLoading = false;

  _handleSaveNote() async {
    setState(() {
      isLoading = true;
    });

    print(
      "note is $_note point is ${widget.point} and marker is ${widget.markerId}",
    );

    await _db.addMarker(_note, widget.point, widget.markerId.value);

    setState(() {
      _note = "";
      isLoading = false;
    });

    widget.onCheesyDialogClose();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Form(
        child: Wrap(
          alignment: WrapAlignment.center,
          spacing: 10.0,
          children: <Widget>[
            Align(
              alignment: Alignment.topRight,
              child: IconButton(
                icon: Icon(Icons.close),
                onPressed: () => widget.onCheesyDialogClose(),
              ),
            ),
            TextFormField(
              decoration: InputDecoration(
                hintText: "Leave a cheesy note...",
                border: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.orange[300],
                  ),
                  borderRadius: BorderRadius.all(
                    Radius.circular(12.0),
                  ),
                ),
              ),
              onChanged: (val) {
                setState(() => _note = val);
              },
              maxLines: null,
              keyboardType: TextInputType.multiline,
            ),
            SizedBox(
              height: 40.0,
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(21.0),
                ),
                onPressed: _handleSaveNote,
                color: Colors.orange[300],
                child: Padding(
                  padding: EdgeInsets.all(15.0),
                  child: isLoading
                      ? Center(
                          child: CircularProgressIndicator(
                            value: 3.0,
                          ),
                        )
                      : Text(
                          'SAVE CHEESE',
                          style: TextStyle(color: Colors.white),
                        ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
