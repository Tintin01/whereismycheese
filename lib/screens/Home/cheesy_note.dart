import 'package:flutter/material.dart';
import 'package:whereismycheese/services/database.dart';
import 'package:flutter_background_geolocation/flutter_background_geolocation.dart'
    as bg;

class CheesyNote extends StatefulWidget {
  final markerId;
  final VoidCallback onCheesyNoteClose;

  CheesyNote({
    @required this.markerId,
    @required this.onCheesyNoteClose,
  });

  @override
  _CheesyNoteState createState() => _CheesyNoteState();
}

class _CheesyNoteState extends State<CheesyNote> {
  final DatabaseService _db = DatabaseService();

  Future<bool> _removeGeofence(String identifier) async {
    return await bg.BackgroundGeolocation.removeGeofence(identifier);
  }

  _handleRemoveNote() async {
    await _db.removeMarker(widget.markerId);
    await _removeGeofence(widget.markerId);
    widget.onCheesyNoteClose();
  }

  Widget _noteContent(AsyncSnapshot snapshot) {
    return Column(
      children: <Widget>[
        Text(snapshot.data["note"]),
        RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(21.0),
          ),
          onPressed: _handleRemoveNote,
          color: Colors.orange[300],
          child: Padding(
            padding: EdgeInsets.all(15.0),
            child: Text(
              'PICK UP CHEESE',
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
      ],
    );
  }

  Widget _noteErrorContent() {
    return Column(
      children: <Widget>[
        Text(
          "Oops! If you're seeing this note you're in a geofence for a marker that no longer exists! 🤷‍♂️ This won't happen again, just hit dismiss",
        ),
        RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(21.0),
          ),
          onPressed: _handleRemoveNote,
          color: Colors.orange[300],
          child: Padding(
            padding: EdgeInsets.all(15.0),
            child: Text(
              'DISMISS 🙅',
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text("You've found a cheesy note!"),
      content: FutureBuilder(
        future: _db.getMarker(widget.markerId),
        builder: (BuildContext context,
            AsyncSnapshot<Map<String, dynamic>> snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.data.isEmpty) {
              return _noteErrorContent();
            }
            return _noteContent(snapshot);
          }
          return Center(
            child: Spacer(),
          );
        },
      ),
    );
  }
}
