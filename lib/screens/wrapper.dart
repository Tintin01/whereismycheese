import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:whereismycheese/models/user.dart';
import 'package:whereismycheese/screens/Home/Home.dart';
import 'package:whereismycheese/screens/authenticate/authenticate.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    final user = Provider.of<User>(context);
    print(user);

    if (user == null) {
      return Authenticate();
    } else {
      return Home();
    }
  }
}
