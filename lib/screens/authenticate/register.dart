import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:whereismycheese/services/auth.dart';
import 'package:whereismycheese/shared/loading.dart';

class Register extends StatefulWidget {
  final Function toggleView;
  Register({this.toggleView});

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final AuthService _auth = AuthService();
  final _formKey = GlobalKey<FormState>();
  bool loading = false;

  String email = "";
  String password = "";
  String error = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
          image: AssetImage("assets/images/dark_map.jpg"),
          fit: BoxFit.cover,
        )),
        child: Stack(
          children: <Widget>[
            Opacity(
              opacity: 0.5,
              child: Container(
                color: Colors.orange[200],
              ),
            ),
            Center(
              child: loading
                  ? Loading()
                  : Card(
                      child: Container(
                        padding: EdgeInsets.all(20.0),
                        height: 520.0,
                        width: 300.0,
                        child: Column(
                          children: <Widget>[
                            Text(
                              "Registration for WhereIsMyCheese",
                              style: GoogleFonts.lato(),
                            ),
                            Image.asset(
                              "assets/images/cheese.png",
                              width: 100.0,
                              height: 200.0,
                            ),
                            Form(
                              key: _formKey,
                              child: Column(
                                children: <Widget>[
                                  TextFormField(
                                    validator: (val) => val.isEmpty
                                        ? 'enter an email address'
                                        : null,
                                    decoration: InputDecoration(
                                      prefixIcon: Icon(Icons.email),
                                      labelText: 'email address',
                                      border: OutlineInputBorder(),
                                    ),
                                    onChanged: (val) {
                                      setState(() => email = val);
                                    },
                                    keyboardType: TextInputType.emailAddress,
                                  ),
                                  SizedBox(height: 10.0),
                                  TextFormField(
                                    validator: (val) => val.length < 6
                                        ? 'password must be at least 6 characters'
                                        : null,
                                    obscureText: true,
                                    decoration: InputDecoration(
                                        prefixIcon: Icon(Icons.security),
                                        labelText: 'password',
                                        border: OutlineInputBorder()),
                                    onChanged: (val) {
                                      setState(() => password = val);
                                    },
                                  ),
                                  SizedBox(height: 10.0),
                                  RaisedButton(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(21.0),
                                    ),
                                    onPressed: () async {
                                      if (_formKey.currentState.validate()) {
                                        setState(() {
                                          loading = true;
                                        });
                                        dynamic result = await _auth
                                            .registerWithEmailAndPassword(
                                                email, password);

                                        if (result == null) {
                                          setState(() {
                                            error =
                                                'please supply a valid email';
                                            loading = false;
                                          });
                                        }
                                      }
                                    },
                                    color: Colors.orange[300],
                                    child: Padding(
                                      padding: EdgeInsets.all(15.0),
                                      child: Text(
                                        'Create! 🧀',
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ),
                                  ),
                                  FlatButton(
                                    onPressed: () {
                                      widget.toggleView();
                                    },
                                    child: Text(
                                      'already have an account?',
                                      style: TextStyle(color: Colors.orange),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
            ),
          ],
        ),
      ),
    );
  }
}
