import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class DatabaseService {
  final String uid;
  DatabaseService({this.uid});
  // collection reference
  final CollectionReference markerCollection =
      Firestore.instance.collection("markers");

  Stream<QuerySnapshot> getMarkers() {
    try {
      return markerCollection.snapshots();
    } catch (err) {
      print("dB error $err");
      return null;
    }
  }

  Future<Map<String, dynamic>> getMarker(String identifier) async {
    try {
      DocumentSnapshot snapshot =
          await markerCollection.document(identifier).get();
      return snapshot.data;
    } catch (err) {
      print("error fetching event $err");
      return null;
    }
  }

  Future addMarker(String note, LatLng location, String id) async {
    try {
      return await markerCollection.document(id).setData({
        'note': note,
        'latitude': location.latitude,
        'longitude': location.longitude,
      });
    } catch (err) {
      print("ERROR ${err.toString()}");
      return null;
    }
  }

  Future removeMarker(String id) async {
    try {
      return await markerCollection.document(id).delete();
    } catch (err) {
      print(err.toString());
      return null;
    }
  }
}
