import 'package:flutter/material.dart';
import 'package:whereismycheese/screens/wrapper.dart';

import 'models/user.dart';
import 'package:provider/provider.dart';
import 'package:whereismycheese/services/auth.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamProvider<User>.value(
      value: AuthService().user,
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.orange,
        ),
        home: Scaffold(
          body: Wrapper(),
        ),
      ),
    );
  }
}
